package org.altervista.diegocaridei.calcolatrice;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.lang.Float;

public class MyActivity extends ActionBarActivity implements View.OnClickListener {

    //Dichiarazioni
    EditText textUno;
    EditText tetNumDue;
    Button bPiu;
    Button bPer;
    Button bDiviso;
    Button bMeno;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my);
        //Setto il tutto
        textUno=(EditText)findViewById(R.id.editText1);
        tetNumDue=(EditText)findViewById(R.id.editText2);
        bPiu =(Button)findViewById(R.id.buttonPiu);
        bPer =(Button)findViewById(R.id.buttonPer);
        bDiviso=(Button)findViewById(R.id.buttonDiviso);
        bMeno =(Button)findViewById(R.id.buttonMeno);

        bPiu.setOnClickListener(this);
        bPer.setOnClickListener(this);
        bDiviso.setOnClickListener(this);
        bMeno.setOnClickListener(this);


    }

    //implemento il metodo onCick dell'interfaccia  OnClickListener
    public void onClick(View v) {
        //Creo un oggetto number che conterrà il risultato delle nostre operazioni
        Number risultato = 0;

        //Lo switch ci consente di determinare quale bottone è stato premuto
        switch (v.getId()) {
            case R.id.buttonPiu:
                try {
                    risultato= Float.parseFloat(textUno.getText().toString())+Float.parseFloat(tetNumDue.getText().toString());
                }
                catch (Exception e){
                    //un Toast è una sorta di pop up che ci consente di mostrare un messaggio momentaneo sul display
                    Toast.makeText(this,"Operazione non valida", Toast.LENGTH_LONG).show();
                    //pulisco i campi di testo
                    textUno.setText("");
                    tetNumDue.setText("");
                }
                break;
            case R.id.buttonMeno:
                try {
                    risultato = Float.parseFloat(textUno.getText().toString()) - Float.parseFloat(tetNumDue.getText().toString());
                }  catch (Exception e){
                    //un Toast è una sorta di pop up che ci consente di mostrare un messaggio momentaneo sul display
                    Toast.makeText(this,"Operazione non valida", Toast.LENGTH_LONG).show();
                    //pulisco i campi di testo
                    textUno.setText("");
                    tetNumDue.setText("");
                }
                break;
            case R.id.buttonPer:
                try {
                    risultato = Float.parseFloat(textUno.getText().toString()) * Float.parseFloat(tetNumDue.getText().toString());
                }  catch (Exception e){
                    //un Toast è una sorta di pop up che ci consente di mostrare un messaggio momentaneo sul display
                    Toast.makeText(this,"Operazione non valida", Toast.LENGTH_LONG).show();
                    //pulisco i campi di testo
                    textUno.setText("");
                    tetNumDue.setText("");
                }

                break;
            case R.id.buttonDiviso:
                try {
                    risultato = Float.parseFloat(textUno.getText().toString()) / Float.parseFloat(tetNumDue.getText().toString());
                }  catch (Exception e){
                    //un Toast è una sorta di pop up che ci consente di mostrare un messaggio momentaneo sul display
                    Toast.makeText(this,"Operazione non valida", Toast.LENGTH_LONG).show();
                    //pulisco i campi di testo
                    textUno.setText("");
                    tetNumDue.setText("");
                }
                break;
            default:
                break;
        }

        //un Toast è una sorta di pop up che ci consente di mostrare un messaggio momentaneo sul display
        Toast.makeText(this, (CharSequence) risultato.toString(), Toast.LENGTH_LONG).show();
        //pulisco i campi di testo
        textUno.setText("");
        tetNumDue.setText("");
    }

    //------ Codice di default

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
